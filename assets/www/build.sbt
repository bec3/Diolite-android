import java.util.regex.Pattern

organization := "com.bec3"
name := """Diolite-front"""

//Disable Scala Nature
crossPaths := false
autoScalaLibrary := false

libraryDependencies += "org.webjars.bower" % "ionic" % "1.2.4"
libraryDependencies += "org.webjars" % "ionicons" % "2.0.1"
libraryDependencies += "org.webjars.npm" % "ngstorage" % "0.3.10"
libraryDependencies += "org.webjars.bower" % "ngCordova" % "0.1.14-alpha"


enablePlugins(SbtWeb)

resourceDirectory in Assets := (baseDirectory.value )
excludeFilter in Assets := new PatternFilter(Pattern.compile("(.git|project|target|lib).*")) || "*.sbt" || "bower.json" || ".bowerrc"

//Repository Credentials
credentials += Credentials(Path.userHome / ".ivy2" / ".credentials")

publishTo := Some("Sonatype Nexus" at "https://bec3.com/nexus/repository/dlite")
