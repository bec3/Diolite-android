(function() {
    'use strict';

    angular
        .module('app')
        .controller('FeaturesController', FeaturesController);

    FeaturesController.$inject = ['$stateParams', '$state', 'featureService', '$timeout'];
    function FeaturesController($stateParams, $state, featureService, $timeout) {
        var vm = this;

        vm.features = featureService.getBoard($stateParams.boardName);
        vm.newFeature = newFeature;
        vm.deleteFeature = deleteFeature;


        ////////////////////////////////////////////////////////////////////////////////

        function newFeature() {
            $state.go('newFeature', {boardName : $stateParams.boardName});
        }

        function deleteFeature(feature, event) {
            featureService.deleteFeature(feature).then(function(features) {
                vm.features = features;
            });
        }
    }
})();