(function() {
    'use strict';

    angular
        .module('app')
        .factory('accelerometerService', accelerometerService);

    accelerometerService.$inject = ['$localStorage', '$ionicPlatform', '$cordovaDeviceMotion'];

    /* @ngInject */
    function accelerometerService($localStorage, $ionicPlatform, $cordovaDeviceMotion) {

        $localStorage.hardware.accelerometer = $localStorage.hardware.accelerometer || {};

        var storage = $localStorage.hardware.accelerometer;

        var service = {
            name : "Accelerometer",
            type : "accelerometer",
            isActuator : false,
            _run : accelerometerRun,
            stop : accelerometerStop
        };

        return service;

        ////////////////

        function accelerometerRun() {
            storage.last = storage.last || {
                x : 0,
                y : 0,
                z : 0
            };

            $ionicPlatform.ready(function() {
                //TODO : to remove when https://github.com/driftyco/ng-cordova/pull/1074#issue-118250678 accepted and lib upgraded.
                if (angular.isUndefined(navigator.accelerometer)) {
                    console.log("Device do not support watchAcceleration");
                    return;
                }


                service.watcher = $cordovaDeviceMotion.watchAcceleration({frequency:100});
                service.watcher.then(null, callBackError, callBack);
            });

            function callBack(acceleration){
                var x = acceleration.x;
                var y = acceleration.y;
                var z = acceleration.z;

                var ox = storage.last.x;
                var oy = storage.last.y;
                var oz = storage.last.z;
                var oNorme = storage.last.norme;

                var norme = Math.sqrt((x-ox)*(x-ox) + (y-oy)*(y-oy) + (z-oz)*(z-oz));

                storage.last = {
                    x : x,
                    y : y,
                    z : z,
                    norme : oNorme
                };


                if (Math.abs(norme) > 5 || storage.last.norme > 5 && Math.abs(norme) < 0.5) {
                    service.send({
                        x : x,
                        y : y,
                        z : z
                    });

                    storage.last.norme = Math.abs(norme);
                    service.status = Math.round(storage.last.norme*100)/100;
                }
            }

            function callBackError(error) {
                console.log("[ERROR] [ACCELEROMETER] : " +  error.code + " : " + error.message);
            }
        }

        function accelerometerStop() {
            if (service.watcher) {
                $cordovaDeviceMotion.clearWatch(service.watcher.watchID);
            }
        }
    }
})();