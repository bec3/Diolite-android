(function() {
    'use strict';

    angular
    .module('app')
    .factory('gpsService', gpsService);

    gpsService.$inject = ['$localStorage', '$ionicPlatform', '$cordovaGeolocation', '$timeout'];

    /* @ngInject */
    function gpsService($localStorage, $ionicPlatform, $cordovaGeolocation, $timeout) {

        $localStorage.hardware.gps = $localStorage.hardware.gps || {};

        var storage = $localStorage.hardware.gps;

        var service = {
            name : "GPS",
            type : "gps",
            isActuator : false,
            _run : gpsRun,
            stop : gpsStop
        };
        return service;

        ////////////////

        function gpsRun() {

            var firstLoop = true;

            $ionicPlatform.ready(function() {
                service.watcher = $cordovaGeolocation.watchPosition({
                    enableHighAccuracy: true
                });
                service.watcher.then(null, callBackError, callBack);
            });

            function callBack(position, timeout) {
                var lat = Math.round(position.coords.latitude*100000)/100000;
                var lon = Math.round(position.coords.longitude*100000)/100000;
                var alt = Math.round(position.coords.altitude*100000)/100000;
                var acc = position.coords.accuracy;
                var ts = position.timestamp;

                if (checkChange()) {
                    service.status = acc;

                    storage.last = {
                        latitude : lat,
                        longitude : lon,
                        altitude : alt,
                        accuracy : acc,
                        timestamp : ts
                    };

                    service.send({
                        'latitude' : lat,
                        'longitude': lon,
                        'altitude' : alt
                    });
                }

                ////////////////////////////////////////////////

                function toRadians(number){
                    return (number * Math.PI / 180);
                }

                function checkChange() {
                    if (firstLoop) {
                        firstLoop = false;
                        return true;
                    }

                    if (timeout) {
                        return true;
                    }

                    var R = 6371000; // metres
                    var φ1 = toRadians(storage.last.latitude);
                    var φ2 = toRadians(lat);
                    var Δφ = toRadians(lat-storage.last.latitude);
                    var Δλ = toRadians(lon-storage.last.longitude);

                    var a = Math.sin(Δφ/2) * Math.sin(Δφ/2) + Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ/2) * Math.sin(Δλ/2);
                    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

                    var d = R * c;

                    if (d > acc/2) {
                        return true;
                    }

                    return false;
                }
            }

            function callBackError(error) {
                console.log("[ERROR] [GPS] : " +  error.code + " : " + error.message);
            }
        }

        function gpsStop() {
            $cordovaGeolocation.clearWatch(service.watcher.watchID);
            delete service.status;
        }
    }
})();