(function() {
    'use strict';

    angular
        .module('app')
        .controller('NewFeatureController', NewFeatureController);

    NewFeatureController.$inject = ['$stateParams', '$filter', '$ionicHistory','featureService'];
    function NewFeatureController($stateParams, $filter, $ionicHistory, featureService) {
        var vm = this;

        vm.types = getTypes();
        vm.add = add;

        vm.newFeature = {
            board : $stateParams.boardName,
            global : true
        };

        ////////////////////////////////////////////////////////////////////////////////

        function add() {
            featureService.newFeature(vm.newFeature).then(callBackOk);

            function callBackOk() {
                $ionicHistory.goBack();
            }
        }

        function getTypes() {
            var types = [];

            angular.forEach(featureService.types, function(value, key){
                if (value.isHardware) {
                    return;
                }

                types.push(key);
            });

            return types;
        }
    }
})();