(function() {
    'use strict';

    angular
        .module('app')
        .factory('smsService', smsService);

    smsService.$inject = ['$localStorage', '$ionicPlatform', '$cordovaSms'];

    /* @ngInject */
    function smsService($localStorage, $ionicPlatform, $cordovaSms) {

        $localStorage.hardware.sms = $localStorage.hardware.sms || {};

        var storage = $localStorage.hardware.sms;

        var service = {
            name : "SMS",
            type : "sms",
            isActuator : true,
            _run : smsRun,
            stop : smsStop,
            setData : smsSetData
        };
        return service;

        ////////////////

        function smsRun() {
            try {
                if (service.data && service.data.id !== 0 && service.data.id !== storage.lastId) {
                    storage.lastId = service.data.id;
                    $ionicPlatform.ready(function() {
                        $cordovaSms.send(service.data.value.recipient, service.data.value.message).then(function() {
                        }, function(error) {
                            console.log("[ERROR] [SMS] : " +  error.code + " : " + error.message);
                        });
                    });
                }
            } catch (error) {
                console.log("Error : ");
                console.log(error);
            }
        }

        function smsStop() {
        }

        function smsSetData(data){
            service.data = data;
            service.run();
        }
    }
})();