(function() {
    'use strict';

    angular
        .module('app')
        .factory('buzzerService', buzzerService);

    buzzerService.$inject = ['$localStorage', '$ionicPlatform', '$cordovaVibration'];

    /* @ngInject */
    function buzzerService($localStorage, $ionicPlatform, $cordovaVibration) {

        $localStorage.hardware.buzzer = $localStorage.hardware.buzzer || {};

        var storage = $localStorage.hardware.buzzer;

        var service = {
            name : "Buzzer",
            type : "buzzer",
            isActuator : true,
            _run : buzzerRun,
            stop : buzzerStop,
            setData : buzzerSetData
        };
        return service;

        ////////////////

        function buzzerRun() {
            if (service.data && service.data.id !== 0 && service.data.id !== storage.lastId) {
                storage.lastId = service.data.id;
                $ionicPlatform.ready(function() {
                    $cordovaVibration.vibrate(parseInt(service.data.value));
                });
            }
        }

        function buzzerStop() {
        }

        function buzzerSetData(data) {
            service.data = data;
            service.run();
        }
    }
})();