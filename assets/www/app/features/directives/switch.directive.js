(function() {
    'use strict';

    angular
        .module('app')
        .directive('dSwitch', dSwitch);

    function dSwitch() {
        return {
            require: ['^dFeature', 'dSwitch'],
            restrict : 'E',
            scope : true,
            controllerAs : 'vm',
            link : linker,
            bindToController : true,
            controller: DSwitchController,
            templateUrl : "app/features/templates/switch.html"
        };

        function linker(scope, element, attrs, ctrls) {
            var featureCtrl = ctrls[0];
            var localCtrl = ctrls[1];

            localCtrl.feature = featureCtrl.feature;

            scope.vm = localCtrl;
            localCtrl.activate();
        }
    }

    function DSwitchController() {
        var vm = this;

        vm.activate = activate;
        vm.send = send;

        //////////////////////////////////////////////////////////////////////////////////////////

        function activate() {
            vm.activated = (vm.feature.data.value=="on")?true:false;
            vm.label = (vm.feature.options.label)?vm.feature.options.label:vm.feature.name;
        }

        function send() {
            vm.feature.send((vm.activated)?"on":"off");
        }
    }
})();