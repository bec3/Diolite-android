(function() {
    'use strict';

    angular
        .module('app')
        .directive('dMsgSender', dMsgSender);

    function dMsgSender() {
        return {
            require: ['^dFeature', 'dMsgSender'],
            restrict : 'E',
            scope : true,
            controllerAs : 'vm',
            link : linker,
            bindToController : true,
            controller: DMsgSender,
            templateUrl : "app/features/templates/msg-sender.html"
        };

        function linker(scope, element, attrs, ctrls) {
            var featureCtrl = ctrls[0];
            var localCtrl = ctrls[1];

            localCtrl.feature = featureCtrl.feature;

            scope.vm = localCtrl;
            localCtrl.activate();
        }
    }

    function DMsgSender() {
        var vm = this;

        vm.activate = activate;
        vm.send = send;

        //////////////////////////////////////////////////////////////////////////////////////////

        function activate() {
            vm.text = vm.feature.data.value;
        }

        function send(text) {
            vm.feature.send(text);
        }
    }
})();