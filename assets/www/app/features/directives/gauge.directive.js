(function() {
    'use strict';

    angular
        .module('app')
        .directive('dGauge', dGauge);

    function dGauge() {
        return {
            require: ['^dFeature', 'dGauge'],
            restrict : 'E',
            scope : true,
            controllerAs : 'vm',
            link : linker,
            controller: controller,
            bindToController : true,
            templateUrl : 'app/features/templates/gauge.html'
        };

        function linker(scope, element, attrs, ctrls) {
            var featureCtrl = ctrls[0];
            var localCtrl = ctrls[1];

            localCtrl.feature = featureCtrl.feature;
            scope.vm = localCtrl;

            ///////////////////////////////////////////////////////////////////////////
        }


        function controller($scope) {

        }
    }
})();