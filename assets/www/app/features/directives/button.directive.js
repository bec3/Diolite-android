(function() {
    'use strict';

    angular
        .module('app')
        .directive('dButton', dButton);

    function dButton() {
        return {
            require: ['^dFeature', 'dButton'],
            restrict : 'E',
            scope : true,
            controllerAs : 'vm',
            link : linker,
            bindToController : true,
            controller: DButtonController,
            templateUrl : "app/features/templates/button.html"
        };

        function linker(scope, element, attrs, ctrls) {
            var featureCtrl = ctrls[0];
            var localCtrl = ctrls[1];

            localCtrl.feature = featureCtrl.feature;
            scope.vm = localCtrl;

            localCtrl.activate();
        }
    }

    function DButtonController() {
        var vm = this;
        vm.activate = activate;

        vm.action = action;


        //////////////////////////////////////////////////////////////////////////////////////////////////////////

        function action(data) {
            vm.feature.send(data);
        }

        function activate() {
            vm.label = (vm.feature.options.label)?vm.feature.options.label:vm.feature.name;
        }
    }

})();