(function() {
    'use strict';

    angular
        .module('app')
        .directive('dMsgReceiver', dMsgReceiver);

    function dMsgReceiver() {
        return {
            require: ['^dFeature', 'dMsgReceiver'],
            restrict : 'E',
            scope : true,
            controllerAs : 'vm',
            link : linker,
            controller: controller,
            bindToController : true,
            templateUrl : 'app/features/templates/msg-receiver.html'
        };

        function linker(scope, element, attrs, ctrls) {
            var featureCtrl = ctrls[0];
            var localCtrl = ctrls[1];

            localCtrl.feature = featureCtrl.feature;
            scope.vm = localCtrl;

            ///////////////////////////////////////////////////////////////////////////
        }


        function controller($scope) {

        }
    }
})();