(function() {
    'use strict';

    angular
        .module('app')
        .directive('dSlider', dSlider);

    function dSlider() {
        return {
            require: ['^dFeature', 'dSlider'],
            restrict : 'E',
            scope : true,
            controllerAs : 'vm',
            link : linker,
            bindToController : true,
            controller: DSliderController,
            templateUrl : "app/features/templates/slider.html"
        };

        function linker(scope, element, attrs, ctrls) {
            var featureCtrl = ctrls[0];
            var localCtrl = ctrls[1];

            localCtrl.feature = featureCtrl.feature;
            scope.vm = localCtrl;

            localCtrl.activate();
        }
    }

    function DSliderController() {
        var vm = this;
        vm.activate = activate;

        vm.change = change;


        //////////////////////////////////////////////////////////////////////////////////////////////////////////

        function change() {
            var value = parseFloat(vm.value);
            vm.feature.send(value);
        }

        function activate() {
            vm.label = (vm.feature.options.label)?vm.feature.options.label:vm.feature.name;
            vm.value = vm.feature.data.value;
        }
    }
})();