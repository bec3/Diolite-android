(function() {
    'use strict';

    angular
        .module('app')
        .directive("dFeature", dFeature);

    function dFeature($compile) {
        return {
            restrict : 'E',
            scope : true,
            bindToController : {
                feature : '='
            },
            link : linker,
            controllerAs : 'vm',
            controller : DFeatureController
        };

        function getTemplate(type) {
            return "<d-" + type + "/>";
        }

        function linker(scope, element, attrs, ctrl) {
            element.html(getTemplate(ctrl.feature.type));
            $compile(element.contents())(scope);
        }
    }

    function DFeatureController() {
        var vm = this;

        if (!vm.feature.options) {
            vm.feature.options = {};
        }
    }
})();