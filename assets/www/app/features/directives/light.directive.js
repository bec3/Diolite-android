(function() {
    'use strict';

    angular
        .module('app')
        .directive('dLight', dLight);

    function dLight() {
        return {
            require: ['^dFeature', 'dLight'],
            restrict : 'E',
            scope : true,
            controllerAs : 'vm',
            link : linker,
            controller: controller,
            bindToController : true,
            templateUrl : 'app/features/templates/light.html'
        };

        function linker(scope, element, attrs, ctrls) {
            var featureCtrl = ctrls[0];
            var localCtrl = ctrls[1];

            localCtrl.feature = featureCtrl.feature;
            scope.vm = localCtrl;

            ///////////////////////////////////////////////////////////////////////////
        }


        function controller($scope) {

        }
    }



})();