(function() {
    'use strict';

    angular
    .module('app')
    .factory('featureService', featureService);

    featureService.$inject = ['$q', '$FEATURE_URL_PATH', 'accelerometerService', 'gpsService', 'buzzerService', 'smsService', 'nfcService', 'connectionService', 'errorService'];
    function featureService($q, $FEATURE_URL_PATH, accelerometerService, gpsService, buzzerService, smsService, nfcService, connectionService, errorService){

        var boards = {};
        var hardwareFeatures = {};
        var types = {
            'button' : {},
            'switch' : {},
            'msg-sender' : {},
            'slider' : {},
            'msg-receiver' : {
                isActuator : true
            },
            'light' : {
                isActuator : true
            },
            'gauge' : {
                isActuator : true
            }
        };

        retrieveHardwareFeatures();
        _getBoards();

        return {
            'types' : types,
            'getBoards' : getBoards,
            'getBoard' : getBoard,
            'newFeature' : newFeature,
            'deleteBoard' : deleteBoard,
            'deleteFeature' : deleteFeature
        };

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

        function getBoards() {
            return _getBoards();
        }

        function _getBoards() {
            // return the promise with a registered callback wich the return value will be injected if another callback is defined
            // (see chained promises)
            return connectionService.get($FEATURE_URL_PATH).then(callBack, callBackError);

            function callBack(response) {

                boards = {};
                connectionService.purge();
                retrieveHardwareFeatures();

                angular.forEach(response.data, function(feature){
                    formatFeature(feature);
                    storeFeature(feature);
                });

                return boards;
            }

            function callBackError(error) {
                return errorService.report("Unable to retrieve objects", error).then(function(){
                    return getBoards();
                }, function(error) {
                    console.log(error);
                });
            }
        }

        function getBoard(board) {
            if (!boards[board]) {
                boards[board] = [];
            }
            return boards[board];
        }

        function deleteBoard(board) {
            var promises = [];
            if (boards[board]) {
                boards[board].forEach(function(feature) {
                    promises.push(deleteFeature(feature));
                });
            }

            return $q.all(promises).then(function(){
                delete boards[board];
            });
        }

        function deleteFeature(feature) {
            var boardId = feature.board;
            var board = boards[boardId];

            if (board && !feature.isHardware) {
                boards[boardId].splice(board.indexOf(feature), 1);
            }

            return connectionService.delete(feature).then(function(){
                return board;
            });
        }

        function newFeature(feature) {

            var prefix = (feature.global)?"global/":(connectionService.config.deviceName + "/");
            var board  = (feature.isHardware)?"hardware":feature.board;

            feature.path = prefix + board;
            feature.isActuator = types[feature.type].isActuator;

            var tmpFeature = {
                name : feature.name,
                path : feature.path,
                type : feature.type,
                details : feature.details?feature.details:""
            };

            return connectionService.post($FEATURE_URL_PATH, tmpFeature).then(callBack);

            function callBack() {
                formatFeature(feature);
                storeFeature(feature);
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////

        function formatFeature(feature) {
            var pathSplitted = feature.path.split('/');

            feature.board = pathSplitted[1];
            feature.device =  pathSplitted[0];

            feature.isActuator = types[feature.type].isActuator;
            feature.isHardware = feature.board === "hardware";

            feature.url = feature.path + "/" + feature.name;
            feature.id = feature.url;

            feature.data = feature.data || {
                id : 0
            };

            if (feature.isActuator) {
                feature.refresh = function() {
                    return connectionService.get($FEATURE_URL_PATH + "/" + feature.url);
                };

                feature.setData = types[feature.type].setData || function(data) {
                    feature.data = data;
                };
            } else {
                feature.send = function(data) {
                    return connectionService.send(feature, {
                        'data' : data
                    });
                };
            }
        }

        function storeFeature(feature) {
            if (feature.device !== "global" && feature.device !== connectionService.config.deviceName) {
                return;
            }

            if (!feature.isHardware) {
                if (!boards[feature.board]) {
                    boards[feature.board] = [];
                }

                boards[feature.board].push(feature);
            } else {
                angular.merge(hardwareFeatures[feature.name], feature);
                hardwareFeatures[feature.name].run();
                hardwareFeatures[feature.name].activate = true;
            }

            if (feature.isActuator) {
                connectionService.register(feature);
            }
        }

        function retrieveHardwareFeatures() {
            boards.hardware = [];
            boards.hardware.push(formatHardwareFeature(accelerometerService));
            boards.hardware.push(formatHardwareFeature(gpsService));
            boards.hardware.push(formatHardwareFeature(buzzerService));
            boards.hardware.push(formatHardwareFeature(smsService));
            boards.hardware.push(formatHardwareFeature(nfcService));
        }

        function formatHardwareFeature(feature) {
            feature.isHardware = true;
            feature.activate = false;
            feature.run = run;
            feature.init = init;
            feature.destroy = destroy;
            feature.change = change;
            types[feature.type] = types[feature.type] || feature;
            hardwareFeatures[feature.name] = feature;

            function run(){
                try {
                    feature._run();
                } catch(error) {
                    console.log("Error on running hardware feature type : " + feature.type);
                    console.log(error);
                    feature.activate = false;
                }
            }

            function init(){
                newFeature(feature);
            }

            function destroy() {
                try {
                    feature.stop();
                } finally {
                    deleteFeature(feature);
                }
            }

            function change(){
                if (feature.activate) {
                    feature.init();
                } else {
                    feature.destroy();
                }
            }

            return feature;
        }
    }

})();