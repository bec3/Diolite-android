(function() {
    'use strict';

    angular
    .module('app')
    .factory('nfcService', nfcService);

    nfcService.$inject = ['$ionicPlatform', '$localStorage'];

    /* @ngInject */
    function nfcService($ionicPlatform, $localStorage) {

        $localStorage.hardware.nfc = $localStorage.hardware.nfc || {};

        var storage = $localStorage.hardware.nfc;

        var service = {
            name : 'NFC',
            type : "msg-sender",
            isActuator : false,
            _run : nfcRun,
            stop : nfcStop
        };
        return service;

        ////////////////

        function nfcRun() {
            $ionicPlatform.ready(function() {
                nfc.addNdefListener(function (nfcEvent) {
                    nfcEvent.tag.ndefMessage.forEach(function(record){

                        var type = nfc.bytesToString(record.type);

                        if (type === "T") {
                            service.send(ndef.textHelper.decodePayload(record.payload)).then(function(bla){
                            });
                        } else if (type === "U") {
                            service.send(ndef.uriHelper.decodePayload(record.payload));
                        } else {
                            service.send("Unknown TNF");
                        }
                    });
                });
            });
        }

        function nfcStop() {
            nfc.removeNdefListener();
        }
    }
})();