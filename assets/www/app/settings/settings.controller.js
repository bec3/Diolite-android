(function() {
    'use strict';

    angular
        .module('app')
        .controller('SettingsController', SettingsController);

    SettingsController.$inject = ['$VERSION', '$rootScope', '$scope', '$state', '$ionicActionSheet', 'loginService', 'connectionService'];

    /* @ngInject */
    function SettingsController($VERSION, $rootScope , $scope, $state, $ionicActionSheet, loginService, connectionService) {
        var vm = this;

        var config = connectionService.config;

        vm.update = update;

        $scope.show = show;

        init();

        $rootScope.setSettingsVisibility = function(visible) {
            $scope.hidden = !visible;
        };

        $rootScope.$on('$stateChangeSuccess', function(event, toState){
            if (toState.name === "settings") {
                init();
            }
        });

        ////////////////

        function show() {
            var title = 'DIoLite ' + $VERSION;

            var configActionSheet = {
                buttons         : [
                    { text: 'Settings' }
                ],
                destructiveText : 'Logout',
                titleText       : title,
                cancelText      : 'Cancel',
                buttonClicked   : function(index) {
                    switch(index) {
                        case 0 : settings();
                        break;
                        default : console.log("wut ?");
                    }
                    return true;
                },
                destructiveButtonClicked : function() {
                    hideSheet();
                    loginService.logout();
                }
            };

            var hideSheet = $ionicActionSheet.show(configActionSheet);

            function settings() {
                $state.go('settings');
            }
        }

        function update() {
            config.rootUrl = angular.copy(vm.url);

            if (!vm.automaticWsUrl) {
                config.wsUrl = angular.copy(vm.wsUrl);
                config.automaticWsUrl = false;
            } else {
                config.wsUrl = undefined;
                config.automaticWsUrl = true;
            }

            config.deviceName = angular.copy(vm.deviceName);
            vm.confirm = false;
            loginService.logout();
        }

        function cancel() {
            init();
            vm.confirm = false;
        }

        function init() {
            vm.wsReady = config.wsReady;
            vm.url = angular.copy(config.rootUrl);
            vm.wsUrl = angular.copy(config.wsUrl);
            vm.deviceName = angular.copy(config.deviceName);
            vm.automaticWsUrl = angular.copy(config.automaticWsUrl);
        }

    }
})();