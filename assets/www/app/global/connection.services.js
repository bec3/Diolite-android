(function() {
    'use strict';

    angular
    .module('app')
    .factory('connectionService', connectionService);

    connectionService.$inject = ['$http', '$timeout', '$ionicPlatform', '$cordovaDevice', '$localStorage', 'errorService', '$FEATURE_URL_PATH', '$DEFAULT_PORT', '$DEFAULT_PROTOCOL', '$REFRESH_HTTPREST_TIMEOUT'];

    /* @ngInject */
    function connectionService($http, $timeout, $ionicPlatform, $cordovaDevice, $localStorage, errorService, $FEATURE_URL_PATH, $DEFAULT_PORT, $DEFAULT_PROTOCOL, $REFRESH_HTTPREST_TIMEOUT) {
        var actuators = {};
        var ws = null;


        $localStorage.config =  $localStorage.config || {
            rootUrl : "",
            authentified : true,
            automaticWsUrl : true,
            deviceName : ""
        };

        var config = $localStorage.config;
        config.authentified = true;
        config.wsReady = false;

        purge();
        run();

        return {
            get : get,
            put : put,
            post: post,
            send: send,
            delete : deleteFunc,
            register : register,
            purge : purge,
            config : $localStorage.config,
            addDomain : addDomain
        };

        ////////////////////////////////////////////////

        function get(url) {
            return $http.get(config.rootUrl + url, {
                responseType : "json"
            });
        }

        function put(url, data) {
            return $http.put(config.rootUrl + url, data, {
                responseType : "json"
            });
        }

        function post(url, data) {
            return $http.post(config.rootUrl + url, data, {
                responseType : "json"
            });
        }

        function send(feature, data) {
            if(ws && ws.readyState === ws.OPEN) {
                data.name = feature.name;
                data.path = feature.path;
                data.details = data.details;
                ws.send(angular.toJson(data));
            } else {
                put($FEATURE_URL_PATH + "/" + feature.url, data);
            }
        }

        function deleteFunc(toDelete) {
            var url = "";

            if ( typeof(toDelete) === "object") {
                url = prepareToDeleteFeature(toDelete);
            } else {
                url = toDelete;
            }

            return $http.delete(config.rootUrl + url, {
                responseType : "json"
            });
        }

        function register(actuator) {
            actuators[actuator.id] = actuators[actuator.id] || actuator;
        }

        function purge() {
            actuators = {};

            if (config.automaticWsUrl) {
                config.wsUrl = null;
            }

            $ionicPlatform.ready(function(){
                if (!config.deviceName) {
                    try {
                        config.deviceName =  $cordovaDevice.getModel().replace(" ", "_").replace(",","_") + "_" + $cordovaDevice.getPlatform().replace(" ", "_").replace(",","_");
                    } catch (error) {
                        console.log("device UUID not available, using standard value");
                        console.log(error.message);
                        config.deviceName = "global";
                    }
                }
            });
        }

        function addDomain(domain) {
            config.rootUrl = config.rootUrl || $DEFAULT_PROTOCOL + "://" + domain + ":" + $DEFAULT_PORT;
        }

        function run() {
            if (ws) {
                config.wsReady = (ws.readyState == ws.OPEN);
            }

            if (config.authentified) {
                if (!ws || ws.url !== config.wsUrl || ws.readyState !== ws.OPEN) {
                    config.wsReady = false;
                    if (ws) {
                        ws.close();
                    }

                    try {
                        if (config.automaticWsUrl) {
                             config.wsUrl = "ws://" + config.rootUrl.split("://")[1] + "/api/stream";
                        }

                        ws = new WebSocket(config.wsUrl);

                        ws.onmessage = function (event) {
                            $timeout(function(){
                                var data = angular.fromJson(event.data);
                                data.id = data.path + "/" + data.name;

                                if (actuators[data.id]) {
                                    actuators[data.id].setData(data.data);
                                }
                            });
                        };
                    } catch (error) {
                        console.log("[ERROR] [WS] " + error);
                    }

                    console.log("[Warning] WS is dans les choux, HTTP update used");

                    var actuatorList = Object.keys(actuators);

                    actuatorList.forEach(function(actuatorK){
                        var actuator = actuators[actuatorK];

                        actuator.refresh().then(function(value){
                            actuator.setData(value.data.data);
                        }, function(error) {
                            if (config.authentified) {
                                errorService.report("Unable to refresh actuators", error);
                            }
                        });
                    });
                }
            }

            $timeout(run, $REFRESH_HTTPREST_TIMEOUT * 1000);
        }

        //////////////////////////////////////

        function prepareToDeleteFeature(feature) {
            if (feature.isActuator) {
                delete actuators[feature.id];
            }

            return $FEATURE_URL_PATH + "/" + feature.url;
        }
    }
})();