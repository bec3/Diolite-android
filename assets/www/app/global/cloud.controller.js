(function() {
    'use strict';

    angular
        .module('app')
        .controller('CloudController', CloudController);

    CloudController.$inject = ['connectionService', '$state', '$rootScope'];

    /* @ngInject */
    function CloudController(connectionService, $state, $rootScope) {
        var vm = this;

        var config = connectionService.config;

        $rootScope.setSettingsVisibility(false);
        $rootScope.hideHardware = true;

        activate();

        ////////////////

        function activate() {
        	config.rootUrl = window.location.href.split("/cloud/index.html")[0];
        	config.automaticWsUrl = false;
        	config.wsUrl = "ws://" + window.location.href.split("://")[1].split("/cloud/index.html")[0] + "/api/stream";
            config.deviceName = "Bec4_cloud";
        	$state.go("boards");
        }
    }
})();