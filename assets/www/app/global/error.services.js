(function() {
    'use strict';

    angular
        .module('app')
        .factory('errorService', errorService);

    errorService.$inject = [];

    /* @ngInject */
    function errorService() {
        var failover = function(label, error){
            console.log("[ERROR] : " + label);
            console.log(angular.toJson(error, true));
        };

        var service = {
            report: report,
            registerFailover : registerFailover
        };
        return service;

        ////////////////

        function report(label, error) {
            return failover(label, error);
        }

        function registerFailover(newFailover) {
            failover = newFailover;
        }
    }
})();