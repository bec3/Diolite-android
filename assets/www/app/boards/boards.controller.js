(function() {
    'use strict';

    angular.module('app')

    .controller('BoardsController', BoardsController);

    BoardsController.$inject = ['$rootScope', '$scope', '$state', '$ionicHistory', 'featureService'];
    function BoardsController($rootScope, $scope, $state, $ionicHistory, featureService) {
        var vm = this;

        vm.boards = null;
        vm.getLength = getLength;

        vm.doRefresh = doRefresh;
        vm.addBoard = addBoard;
        vm.deleteBoard = deleteBoard;
        vm.loadBoard = loadBoard;

        $rootScope.$on('loggedIn', function(){
            activate();
        });

        $ionicHistory.clearHistory();
        activate();

        ////////////////////////////////////////////////////////////////////////////////////////////////////

        function addBoard(valid) {
            if (valid && vm.newBoardName && vm.newBoardName !== "" && !vm.boards[vm.newBoardName]) {
                vm.boards[vm.newBoardName] = [];
                vm.newBoardName = "";
            }
        }

        function deleteBoard(id, event) {
            featureService.deleteBoard(id).then(function(){
                if (getLength() === 0) {
                    vm.showDeleteBtns = false;
                }
                activate();
            });

            event.stopPropagation();
        }

        function loadBoard(board) {
            if(!vm.showDeleteBtns) {
                $state.go('features', {boardName : board});
            }
        }

        function getLength() {
            if (vm.boards) {
                return Object.keys(vm.boards).length - 1;
            } else {
                return 0;
            }
        }

        function activate() {
            vm.hideHardware = $rootScope.hideHardware;

            return featureService.getBoards().then(callBackOk);

            function callBackOk(data) {
                vm.boards = data;
            }
        }

        function doRefresh() {
            activate().then(function(){
                $scope.$broadcast('scroll.refreshComplete');
            });
        }

    }
})();