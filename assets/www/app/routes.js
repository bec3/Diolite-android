(function() {
    'use strict';

    angular
        .module('app')
        .config(route);

    route.$inject = ['$stateProvider', '$urlRouterProvider'];
    function route($stateProvider, $urlRouterProvider) {

        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js
        $stateProvider

        .state('login', {
            url: '/login',
            templateUrl: 'app/login/templates/login.html',
            controller: 'LoginController as loginVm'
        })

        .state('boards', {
            url: '/boards',
            templateUrl: 'app/boards/templates/boards.html',
            controller: 'BoardsController as boardsVm'
        })

        .state('features', {
            url: '/boards/:boardName',
            templateUrl: 'app/features/templates/features.html',
            controller: 'FeaturesController as featuresVm'
        })

        .state('newFeature', {
            url: '/boards/:boardName/new',
            templateUrl: 'app/features/templates/newFeature.html',
            controller: 'NewFeatureController as newFeatureVm'
        })

        .state('cloud', {
            url: '/cloud',
            controller: 'CloudController'
        })

        .state('settings', {
            url: '/settings',
            templateUrl: 'app/settings/templates/settings.html',
            controller: 'SettingsController as settingsVm'
        });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/boards');
    }

})();