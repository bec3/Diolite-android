(function() {
    'use strict';

    angular
        .module('app')
        .factory('loginService', loginService);

    loginService.$inject = ['$http', '$rootScope', '$state', '$localStorage', '$q', '$LOGIN_URL_PATH', 'errorService', 'connectionService'];
    function loginService($http, $rootScope, $state, $localStorage, $q, $LOGIN_URL_PATH, errorService, connectionService){
        var account = $localStorage.login || {};
        var error = {
            label : "",
            detail : {}
        };

        var graphicDefered;

        errorService.registerFailover(issue);

        return {
            'login'     : login,
            'logout'    : logout,
            'error'     : error,
            'graphicDefered' : graphicDefered
        };

        //////////////////////////////////////////////////////////////////

        function login(mail, pwd) {
            return _login(mail, pwd);
        }


        function logout() {
            connectionService.config.authentified = false;

            error.label = "";
            error.detail = {};

            $state.go("login");

            return connectionService.delete($LOGIN_URL_PATH);
        }

        ////////////////////////////////////////////////////////////////////////

        function authentified() {
            connectionService.config.authentified = true;

            error.label = "";
            error.detail = {};

            if (graphicDefered) {
                graphicDefered.resolve();
            }
        }


        function loginGraphic() {
            graphicDefered = $q.defer();
            $state.go("login");

            return graphicDefered.promise;
        }

        function _login(mail, pwd) {
            if (!mail) {
                return loginGraphic();
            }

            account.mail = mail;
            account.pwd = pwd;

            var detail = mail.split('@');

            connectionService.addDomain(detail[1]);

            return connectionService.post($LOGIN_URL_PATH, {
                "username"  : detail[0],
                "password"  : pwd,
                "service"   : detail[1]
            }).then(function() {
                connectionService.purge();
                authentified();
                $rootScope.$broadcast("loggedIn");
            }, function(error){
                console.log("[ERROR] [Login] fail to log in");
                if ($state.is('login') && graphicDefered) {
                    return errorService.report("Impossible to connect", error);
                } else {
                    return loginGraphic();
                }
            });
        }

        function issue(label, detail) {
            connectionService.config.authentified = false;

            var promise = _login(account.mail, account.pwd);

            if (account.mail && account.pwd) {
                error.label = label;
                error.detail = detail;
                account = {};
            }

            return promise;
        }
    }
})();