(function() {
    'use strict';

    angular.module('app').controller('LoginController', LoginController);

    LoginController.$inject = ['$state', '$ionicHistory', '$localStorage', '$timeout', 'loginService'];
    function LoginController($state, $ionicHistory, $localStorage, $timeout, loginService) {
        var vm = this;

        vm.login = $localStorage.login || {};

        vm.connect = connect;

        vm.loginError = loginService.error;

        $ionicHistory.clearHistory();

        ////////////////////////////////////////////////////////////////////////////////////////////////////

        function connect() {
            loginService.login(vm.login.mail,vm.login.pwd).then(loginCallbackOk, loginCallbackError);

            $localStorage.login = {
                mail : vm.login.mail,
                pwd : vm.login.pwd
            };

            function loginCallbackOk() {
                $state.go("boards");
            }

            function loginCallbackError(error) {
                console.log(angular.toJson(error, true));
            }
        }
    }
})();